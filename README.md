### Yelp Test App

The goal of this project is to provide some simple functions built on top of Yelp data.
*N.B. As general and very important note, please be aware that the code is intended just for demonstration purposes.
 The code is not by any means ready for production or with full test coverage*

# How to run the app
Run the Main in
```
ny.Main
```

Possible parameters are:
```
      "basePath" - required, the input to the folder containing YELP datasets
      "service"  - required, service to run. One among TopNUsers, City, LatLon, OutputLocations
      "city"     - optional, city for BusinessService.getAllResturantsInCity() service
      "latLon"   - optional, location for BusinessServicegetResturantsCloseBy() service
      "output"   - optional, output file for Business.service.saveLocations() service
      "n"        - optional, N for UserService.topNUsersByReviews() service
```

# Services
The following are the list of services available at the moment:

```
BusinessService.getAllResturantsInCity(city: String)
```
retrieve all the restaurants in a given city

```
BusinessServicegetResturantsCloseBy(latLon: LatLon, radius: Double)
```
retrieve all the restaurants close to a specific location


```
Business.service.saveLocations(outputFile: String)
```
store all the businesses with lat long in a CSV file (useful for visualization)


```
UserService.topNUsersByReviews(n: Int)
```
retrieve the Top N users by numbers of reviews
