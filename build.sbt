name := "new-yorker"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.2.0"
libraryDependencies += "org.apache.spark" % "spark-streaming_2.11" % "2.2.0"
libraryDependencies += "org.apache.spark" % "spark-streaming-kafka-0-8_2.11" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.2.0"
libraryDependencies +=  "org.apache.spark" %% "spark-hive" % "2.2.0"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0"

dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.6.3"
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.3"
dependencyOverrides += "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.6.3"
dependencyOverrides += "com.google.guava" % "guava" % "16.0.1"
        