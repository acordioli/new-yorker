package ny

import ny.Services.Services
import org.apache.spark.sql.{Row, SparkSession}
import ny.services.{BusinessService, UserService}
import ny.utils.{LatLon, NewYorkerConfig}


object Main {

  def printBusiness(rows: Array[Row]): Unit = {
    rows.foreach(r => println(s"${r.getAs("name")}, ${r.getAs("address")}"))
  }

  def printUsers(rows: Array[Row]): Unit = {
    rows.foreach(r => println(s"${r.getAs("name")}, ${r.getAs("review_count")}"))
  }

  val spark = SparkSession
    .builder()
    .master("local")
    .appName("NewYorker Spark Example")
    .getOrCreate()


  def main(args: Array[String]): Unit = {

    val parser = new scopt.OptionParser[NewYorkerConfig]("New Yorker Test App") {
      head("New Yorker Test App", "0.1")

      opt[String]("basePath").required().action((s, conf) => conf.copy(basePath = s))
        .text("tar is the path to input tar file")
      opt[Services]("service").required().action((s, conf) => conf.copy(service = s))
        .text("service to run (TopNUsers, City, LatLon, OutputLocations)")
      opt[String]("city").optional().action((c, conf) => conf.copy(city = Some(c)))
        .text("city for BusinessService.getAllResturantsInCity() service")
      opt[String]("latLon").optional().action { (latLon, conf) =>
        val lat = latLon.split(",")(0).toDouble
        val lon = latLon.split(",")(1).toDouble
        conf.copy(latLon = Some(LatLon(lat, lon)))
      }.text("location for BusinessServicegetResturantsCloseBy() service")
      opt[String]("output").optional().action((o, conf) => conf.copy(outputFile = Some(o)))
        .text("output file for Business.service.saveLocations() service")
      opt[Int]("n").optional().action((n, conf) => conf.copy(n = Some(n)))
        .text("N for UserService.topNUsersByReviews() service")

    }

    parser.parse(args, NewYorkerConfig(basePath = "", service = Services.TopNUsers, None, None, None, None)) match {
      case Some(config) =>

        println("New Yorker Test App")

        config.service match {
          case Services.City =>
            val businessService = new BusinessService(spark, config)
            val result = businessService.getAllRestaurantsInCity(config.city.get)
            printBusiness(result)

          case Services.LatLon =>
            val businessService = new BusinessService(spark, config)
            val result = businessService.getRestaurantsCloseBy(config.latLon.get, DefaultRadius)
            printBusiness(result)

          case Services.OutputLocations =>
            val businessService = new BusinessService(spark, config)
            businessService.saveLocations(config.outputFile.get)

          case Services.TopNUsers =>
            val userService = new UserService(spark, config)
            val result = userService.topNUsersByReviews(config.n.get)
            printUsers(result)

          case _ =>
            throw new IllegalArgumentException()
        }


      case None =>

    }


  }

}
