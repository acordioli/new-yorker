package object ny {

  val DefaultRadius = 1000

  object Services extends Enumeration {
    type Services = Value
    val TopNUsers, City, LatLon, OutputLocations = Value
  }

  implicit val servicesRead: scopt.Read[Services.Value] =
    scopt.Read.reads(Services withName _)


}
