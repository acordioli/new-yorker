package ny.readers

import org.apache.spark.sql.{DataFrame, SparkSession}


class Reader(spark: SparkSession, basePath: String) {

  def readBusiness(): DataFrame = {
    spark.read.json(s"${basePath}/business.json")
  }

  def readCheckIn(): DataFrame = {
    spark.read.json(s"${basePath}/checkin.json")
  }

  def readUser(): DataFrame = {
    spark.read.json(s"${basePath}/user.json")
  }

}
