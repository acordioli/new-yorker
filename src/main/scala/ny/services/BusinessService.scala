package ny.services

import org.apache.spark.sql.{Row, SparkSession}
import ny.readers.Reader
import ny.utils.{LatLon, NewYorkerConfig}
import org.apache.spark.sql.functions._

class BusinessService(spark: SparkSession, conf: NewYorkerConfig) {

  val df = new Reader(spark, conf.basePath).readBusiness()

  import ny.utils.GeoUtils
  def withinUDF = udf(GeoUtils.within _)

  /**
    * Select all the resturants in a given city
    * @param city the city
    * @return list of results
    */
  def getAllRestaurantsInCity(city: String): Array[Row] = {
    df.where(df("city") === city).collect()
  }

  /**
    * Select all the restaurants nearby a specific location
    * @param latLon lat and long
    * @param radius radius
    * @return list of results
    */
  def getRestaurantsCloseBy(latLon: LatLon, radius: Double): Array[Row] = {
    df.where(withinUDF(col("latitude"), col("longitude"), lit(latLon.lat), lit(latLon.lon), lit(radius))).collect()
  }

  /**
    * Save all the business locations to a specific path in CSV format
    * @param outputLocation
    */
  def saveLocations(outputLocation: String): Unit = {
    df.select(col("business_id"), col("latitude"), col("longitude"))
      .write.format("com.databricks.spark.csv").save(outputLocation)

  }
}
