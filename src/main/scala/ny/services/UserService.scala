package ny.services

import ny.readers.Reader
import ny.utils.NewYorkerConfig
import org.apache.spark.sql.{Row, SparkSession}

class UserService(spark: SparkSession, conf: NewYorkerConfig) {

  val df = new Reader(spark, conf.basePath).readUser()

  /**
    * Select the TOP N users by number of reviews
    * @param n
    * @return list of results
    */
  def topNUsersByReviews(n: Int): Array[Row] = {

    import org.apache.spark.sql.functions._

    df.sort(desc("review_count")).take(n)

  }

}
