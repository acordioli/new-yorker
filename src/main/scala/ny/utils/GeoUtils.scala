package ny.utils

object GeoUtils {

  /**
    * Compute distance in meters between two points in lat long format
    * @param lat1
    * @param lng1
    * @param lat2
    * @param lng2
    * @return
    */
  def distFrom(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Double  = {
    val earthRadius = 6371000 // in meters
    val dLat = Math.toRadians(lat2-lat1);
    val dLng = Math.toRadians(lng2-lng1);
    val a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
        Math.sin(dLng/2) * Math.sin(dLng/2);
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    earthRadius * c
  }

  def within(lat1: Double, lon1: Double, lat2: Double, lng2: Double, radius: Double): Boolean = {
    (distFrom(lat1, lon1, lat2, lng2) / 2) < radius
  }

}
