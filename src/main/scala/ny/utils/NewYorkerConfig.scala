package ny.utils

import ny.Services.Services

case class LatLon(lat: Double, lon: Double)

case class NewYorkerConfig(basePath: String, service: Services, city: Option[String], n: Option[Int],
                           latLon: Option[LatLon], outputFile: Option[String])
